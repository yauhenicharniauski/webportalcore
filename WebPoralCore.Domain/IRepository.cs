﻿using System;
using System.Collections.Generic;
using System.Text;
using WebPoralCore.Domain.Entities;

namespace WebPoralCore.Domain
{
    public interface IRepository
    {
	    User GetUserById(int id);
	    User GetUserByEmail(string email);
	    User Authenticate(string email, string password);
	    User Register(string email, string password);
	    IEnumerable<Role> GetRolesByUserId(int id);
	    IEnumerable<Product> GetProducts();
	}
}
