﻿using System;
using System.Collections.Generic;
using System.Text;

namespace WebPoralCore.Domain.Entities
{
    public sealed class User
    {
	    public int ID { get; set; }
	    public string Email { get; set; }
	    public string Password { get; set; }
	}
}
