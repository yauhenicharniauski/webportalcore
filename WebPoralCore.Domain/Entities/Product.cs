﻿using System;
using System.Collections.Generic;
using System.Text;

namespace WebPoralCore.Domain.Entities
{
    public sealed class Product
    {
	    public int ID { get; set; }
	    public string Title { get; set; }
	    public int Unit { get; set; }
		public decimal Price { get; set; }
	}
}
