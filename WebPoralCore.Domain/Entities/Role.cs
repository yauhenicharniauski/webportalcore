﻿using System;
using System.Collections.Generic;
using System.Text;

namespace WebPoralCore.Domain.Entities
{	
	public sealed class Role
	{
		public const string ROLE_ADMINISTRATOR = "Admin";
		public const string ROLE_EDITOR = "Editor";

		public int ID { get; set; }
		public int UserID { get; set; }
		public string Title { get; set; }
	}
}
