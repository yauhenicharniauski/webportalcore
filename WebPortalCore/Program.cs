﻿using System;
using System.Collections.Generic;
using System.IO;
using System.Linq;
using System.Threading.Tasks;
using Microsoft.AspNetCore;
using Microsoft.AspNetCore.Hosting;
using Microsoft.Extensions.Configuration;
using Microsoft.Extensions.DependencyInjection;
using Microsoft.Extensions.Logging;
using WebPortalCore.DataAccess;

namespace WebPortalCore
{
    public class Program
    {
        public static void Main(string[] args)
        {
	        var host = BuildWebHost(args);
	        using (var scope = host.Services.CreateScope())
	        {		        
				var services = scope.ServiceProvider;
		        var logger = services.GetRequiredService<ILogger<Program>>();
				TrySetupDb(services, logger);
	        }

			host.Run();
        }

	    public static void TrySetupDb(IServiceProvider services, ILogger<Program> logger)
	    {
		    try
		    {
			    var context = services.GetRequiredService<PortalDbContext>();
			    var success = PortalDbSetup.TryInitDb(context);
			    if (success)
			    {
				    logger.LogInformation("Database successfully setup");
			    }
		    }
		    catch (Exception ex)
		    {
			    logger.LogError(ex, "An error occurred while creating the database.");
		    }
		}

		public static IWebHost BuildWebHost(string[] args) =>
            WebHost.CreateDefaultBuilder(args)
                .UseStartup<Startup>()
                .Build();
    }
}
