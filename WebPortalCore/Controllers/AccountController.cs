﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Security.Claims;
using System.Threading.Tasks;
using Microsoft.AspNetCore.Authentication;
using Microsoft.AspNetCore.Authentication.Cookies;
using Microsoft.AspNetCore.Mvc;
using WebPoralCore.Domain;
using WebPoralCore.Domain.Entities;
using WebPortalCore.Models;

namespace WebPortalCore.Controllers
{
    public class AccountController : Controller
    {
	    private readonly IRepository _rep;
	    public AccountController(IRepository rep)
	    {
		    _rep = rep;
	    }

	    [HttpGet]
	    public IActionResult AccessDenied()
	    {
		    return View();
	    }

		[HttpGet]
	    public IActionResult Login()
	    {
		    return View();
	    }

	    [HttpPost]
	    [ValidateAntiForgeryToken]
	    public async Task<IActionResult> Login(LoginViewModel model)
	    {
		    if (ModelState.IsValid)
		    {
			    var user = _rep.Authenticate(model.Email, model.Password);
			    if (user != null)
			    {
				    await Authenticate(user);
				    return RedirectToAction("Index", "Home");
			    }
			    ModelState.AddModelError("", "Incorrect login or password");
		    }
		    return View(model);
	    }

	    [HttpGet]
	    public IActionResult Register()
	    {
		    return View();
	    }

	    [HttpPost]
	    [ValidateAntiForgeryToken]
	    public async Task<IActionResult> Register(RegisterViewModel model)
	    {
		    if (ModelState.IsValid)
		    {
			    var user = _rep.GetUserByEmail(model.Email);
			    if (user == null)
			    {
				    user = _rep.Register(email: model.Email, password: model.Password);
				    await Authenticate(user);

				    return RedirectToAction("Index", "Home");
			    }
			    else
				    ModelState.AddModelError("", "Incorrect login or password");
		    }
		    return View(model);
	    }

	    private async Task Authenticate(User user)
	    {
		    var claims = new List<Claim>
		    {
			    new Claim(ClaimsIdentity.DefaultNameClaimType, user.Email)
		    };


		    foreach (var role in _rep.GetRolesByUserId(user.ID))
		    {
			    claims.Add(new Claim(ClaimTypes.Role, role.Title));
			}

		    var id = new ClaimsIdentity(claims, "ApplicationCookie", ClaimsIdentity.DefaultNameClaimType, ClaimsIdentity.DefaultRoleClaimType);
		    await HttpContext.SignInAsync(CookieAuthenticationDefaults.AuthenticationScheme, new ClaimsPrincipal(id));
	    }

	    public async Task<IActionResult> Logout()
	    {
		    await HttpContext.SignOutAsync(CookieAuthenticationDefaults.AuthenticationScheme);
		    return RedirectToAction("Login", "Account");
	    }
	}
}