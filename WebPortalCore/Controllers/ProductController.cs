﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;
using Microsoft.AspNetCore.Authorization;
using Microsoft.AspNetCore.Http;
using Microsoft.AspNetCore.Mvc;
using WebPoralCore.Domain;

namespace WebPortalCore.Controllers
{
    public class ProductController : Controller
    {
	    private readonly IRepository _rep;
	    public ProductController(IRepository rep)
	    {
		    _rep = rep;
	    }

		// GET: Product
		public ActionResult Index()
		{
			ViewBag.Products = _rep.GetProducts();
			return View();
        }

        // GET: Product/Details/5
        public ActionResult Details(int id)
        {
            return View();
        }

		// GET: Product/Create
		[Authorize(Roles = "Admin")]
		public ActionResult Create()
        {
            return View();
        }

		// POST: Product/Create
		[Authorize(Roles = "Admin")]
		[HttpPost]
        [ValidateAntiForgeryToken]
        public ActionResult Create(IFormCollection collection)
        {
            try
            {
                // TODO: Add insert logic here

                return RedirectToAction(nameof(Index));
            }
            catch
            {
                return View();
            }
        }

		// GET: Product/Edit/5
		[Authorize(Roles = "Admin")]
		public ActionResult Edit(int id)
        {
            return View();
        }

		// POST: Product/Edit/5
		[Authorize(Roles = "Admin")]
		[HttpPost]
        [ValidateAntiForgeryToken]
        public ActionResult Edit(int id, IFormCollection collection)
        {
            try
            {
                // TODO: Add update logic here

                return RedirectToAction(nameof(Index));
            }
            catch
            {
                return View();
            }
        }

		// GET: Product/Delete/5
		[Authorize(Roles = "Admin")]
		public ActionResult Delete(int id)
        {
            return View();
        }

		// POST: Product/Delete/5
		[Authorize(Roles = "Admin")]
		[HttpPost]
        [ValidateAntiForgeryToken]
        public ActionResult Delete(int id, IFormCollection collection)
        {
            try
            {
                // TODO: Add delete logic here

                return RedirectToAction(nameof(Index));
            }
            catch
            {
                return View();
            }
        }

	    [HttpPost]
	    [ValidateAntiForgeryToken]
	    public ActionResult Buy(int id, IFormCollection collection)
	    {
		    try
		    {
			    // TODO: Add delete logic here

			    return RedirectToAction(nameof(Index));
		    }
		    catch
		    {
			    return View();
		    }
	    }
	}
}