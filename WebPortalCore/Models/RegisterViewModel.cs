﻿using System;
using System.Collections.Generic;
using System.ComponentModel.DataAnnotations;
using System.Linq;
using System.Threading.Tasks;

namespace WebPortalCore.Models
{
    public class RegisterViewModel
    {
	    [Required(ErrorMessage = "Email not specified")]
	    public string Email { get; set; }

	    [Required(ErrorMessage = "Password not specified")]
	    [DataType(DataType.Password)]
	    public string Password { get; set; }

	    [DataType(DataType.Password)]
	    [Compare("Password", ErrorMessage = "Password is incorrect")]
	    public string ConfirmPassword { get; set; }
	}
}
