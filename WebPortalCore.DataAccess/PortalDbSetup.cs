﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using WebPoralCore.Domain;
using WebPoralCore.Domain.Entities;

namespace WebPortalCore.DataAccess
{
    public static class PortalDbSetup
    {
	    public static bool TryInitDb(PortalDbContext context)
	    {
		    context.Database.EnsureCreated();

		    var admin = context.Users.FirstOrDefault(u => u.Email.ToLower() == "admin@gmail.com");
		    if (admin == null)
		    {
			    admin = new User {Email = "admin@gmail.com", Password = "12345"};
			    context.Users.Add(admin);
			    context.SaveChanges();
			}
			
		    var regularUser = context.Users.FirstOrDefault(u => u.Email.ToLower() == "user@gmail.com");
			if (regularUser == null)
			{
				regularUser = new User {Email = "user@gmail.com", Password = "12345"};
				context.Users.Add(regularUser);
				context.SaveChanges();
			};
		    

		    var adminRole = context.Roles.FirstOrDefault(r => r.Title == Role.ROLE_ADMINISTRATOR && r.UserID == admin.ID);
		    if (adminRole == null)
		    {
			    context.Roles.Add(new Role {UserID = admin.ID, Title = Role.ROLE_ADMINISTRATOR});
			    context.SaveChanges();
			}

			if (!context.Products.Any())
		    {
			    context.Products.Add(new Product { Title= "HDD 1TB", Unit = 55, Price = (decimal)74.09 });
			    context.Products.Add(new Product { Title = "HDD 2TB", Unit = 25, Price = (decimal)120.09 });
				context.Products.Add(new Product { Title = "SSD 512GB", Unit = 102, Price = (decimal)190.99 });
			    context.Products.Add(new Product { Title = "SSD 256GB", Unit = 88, Price = (decimal)100.99 });
			    context.Products.Add(new Product { Title = "RAM 8GB", Unit = 47, Price = (decimal)69.99 });
			    context.SaveChanges();
			}

		    return true;
	    }
    }
}
