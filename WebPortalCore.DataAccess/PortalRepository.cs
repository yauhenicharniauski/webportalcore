﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using WebPoralCore.Domain;
using WebPoralCore.Domain.Entities;

namespace WebPortalCore.DataAccess
{
    public sealed class PortalRepository: IRepository
    {
	    private readonly PortalDbContext _db;
	    public PortalRepository(PortalDbContext db)
	    {
		    _db = db;
	    }

	    public User GetUserById(int id)
	    {
		    return _db.Users.FirstOrDefault(x => x.ID == id);
	    }

	    public User GetUserByEmail(string email)
	    {
		    return _db.Users.FirstOrDefault(x => x.Email == email);
	    }

	    public User Authenticate(string email, string password)
	    {
		    if (string.IsNullOrEmpty(email)) return null;

		    var emailLower = email.ToLower();
		    var user = _db.Users.FirstOrDefault(x => x.Email == emailLower);
		    if (user == null) return null;

		    if (user.Password != password) return null;

		    return user;
	    }

	    public User Register(string email, string password)
	    {
		    var user = new User { Email = email.Trim().ToLower(), Password = password };
		    _db.Users.Add(user);
		    _db.SaveChanges();
		    return user;
	    }

	    public IEnumerable<Role> GetRolesByUserId(int id)
	    {
		    return _db.Roles.Where(r => r.UserID == id).ToList();
	    }

	    public IEnumerable<Product> GetProducts()
	    {
		    return _db.Products.ToList();
	    }
	}
}
