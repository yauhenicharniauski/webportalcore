﻿using System;
using System.Collections.Generic;
using System.Text;
using Microsoft.EntityFrameworkCore;
using WebPoralCore.Domain.Entities;

namespace WebPortalCore.DataAccess
{
	public class PortalDbContext : DbContext
	{
		public PortalDbContext(DbContextOptions<PortalDbContext> options) : base(options)
		{
		}

		public DbSet<User> Users { get; set; }
		public DbSet<Role> Roles { get; set; }
		public DbSet<Product> Products { get; set; }

		protected override void OnModelCreating(ModelBuilder modelBuilder)
		{
			modelBuilder.Entity<User>().ToTable("User");
			modelBuilder.Entity<Role>().ToTable("Role");
			modelBuilder.Entity<Product>().ToTable("Product");
		}
	}
}
